module.exports = function(grunt) {
    require('jit-grunt')(grunt);

    grunt.initConfig({
        less: {
            development: {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    "public/css/style.css": "assets/css/src/style.less" // Define o destino e arquivos de origem
                }
            }
        },
        watch: {
            styles: {
                files: ['assets/css/**/*.less'], // Define os arquivos para serem monitorados
                tasks: ['less'],
                options: {
                    nospawn: true
                }
            }
        }
    });

    grunt.registerTask('default', ['less', 'watch']);
};